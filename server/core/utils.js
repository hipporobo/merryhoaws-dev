module.exports = {
  getLogTime: function(){
    var d = new Date();
    var txtDate = '['+d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()+']';
    return txtDate;
  }
}
