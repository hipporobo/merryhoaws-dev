const express = require('express');
const mongoose = require('mongoose');

const User = require('../models/User');
const Plant = require('../models/Plant');
const Session = require('../models/Session');
const Message = require('../core/message-response');
const logger = require('../core/logger');

const router = express.Router();

var SECRET = 'MerryHoaGame';

//for DEBUG
router.get('/plant/randomRateTest', function(req, res) {
    Plant.find({}, findRatio);

    function findRatio(err, plantDocs) {
        if (err) {
            res.json(err);
        } else {
            var sum = 0;
            var noOfData = plantDocs.length;
            var dataRatio = [];
            for (var i = 0; i < noOfData; i++) {
                sum = sum + plantDocs[i].dropRate;
            }

            var index = 0;
            for (var i = 0; i < noOfData; i++) {
                var endDrop = index + Math.round((plantDocs[i].dropRate / sum) * 10000);
                var plantDropRate = {
                    'flowerID': plantDocs[i].flowerID,
                    'startDrop': index,
                    'endDrop': endDrop,
                    'dropRatePercent': parseFloat((endDrop - index) / 100).toFixed(2) + "%"
                };
                index = endDrop;
                dataRatio.push(plantDropRate);
            }

            res.json({
                'sumOfDropRate': sum,
                'noOfData': noOfData,
                'dataRatio': dataRatio,
            });

        }
    }
});

router.get('/plant/randomTest', function(req, res) {
    Plant.find({}, findRatio);

    function findRatio(err, plantDocs) {
        if (err) {
            res.json(err);
        } else {
            var sum = 0;
            var noOfData = plantDocs.length;
            var dataRatio = [];
            for (var i = 0; i < noOfData; i++) {
                sum = sum + plantDocs[i].dropRate;
            }
            plantDocs.sort(function(a, b) {
                return 0.5 - Math.random()
            });
            var index = 0;
            for (var i = 0; i < noOfData; i++) {
                var endDrop = index + Math.round((plantDocs[i].dropRate / sum) * 10000);
                var plantDropRate = {
                    'flowerID': plantDocs[i].flowerID,
                    'startDrop': index,
                    'endDrop': endDrop
                };
                index = endDrop;
                dataRatio.push(plantDropRate);
            }
            var plntIdx = [];
            plntIdx[0] = Math.round((Math.random() * 9876533) % endDrop);
            plntIdx[1] = Math.round((Math.random() * 4332123) % endDrop);
            plntIdx[2] = Math.round((Math.random() * 7675913) % endDrop);
            var plantDrop = [];
            for (var roundRand = 0; roundRand < 3; roundRand++) {
                for (var i = 0; i < dataRatio.length; i++) {
                    if (plntIdx[roundRand] >= dataRatio[i].startDrop && plntIdx[roundRand] <= dataRatio[i].endDrop) {
                        plantDrop.push({
                            'flowerID': dataRatio[i].flowerID,
                            'randomIdx': plntIdx[roundRand]
                        });
                        break;
                    }
                }
            }

            res.json(new Message(req.url, 'Y', plantDrop));
        }
    }
});

router.post('/plant/random', function(req, res) {
    try {
        var sessionId = req.body.sessionId;
        Session.findOne({
            'sessionId': sessionId
        }, function(err, sesDocs) {
            if (err || !sesDocs) {
                res.status(500).json(new Message(req.url, 'error', 'can not get session'));
            } else {
                var plantRandomTemp;
                //get plantRandom from session
                if (sesDocs.data) {
                    if (sesDocs.data.plantRandom) {
                        plantRandomTemp = sesDocs.data.plantRandom;
                    }
                }
                if (plantRandomTemp) { //has random session
                    res.json(new Message(req.url, 'Y', plantRandomTemp));
                } else { //not found session
                    // to do new random
                    Plant.find({}, function(err, plantDocs) {
                        if (err) {
                            res.json(err);
                        } else {
                            var sum = 0;
                            var noOfData = plantDocs.length;
                            var dataRatio = [];
                            for (var i = 0; i < noOfData; i++) {
                                sum = sum + plantDocs[i].dropRate;
                            }
                            plantDocs.sort(function(a, b) {
                                return 0.5 - Math.random()
                            });
                            var index = 0;
                            for (var i = 0; i < noOfData; i++) {
                                var endDrop = index + Math.round((plantDocs[i].dropRate / sum) * 10000);
                                var plantDropRate = {
                                    'flowerID': plantDocs[i].flowerID,
                                    'startDrop': index,
                                    'endDrop': endDrop
                                };
                                index = endDrop;
                                dataRatio.push(plantDropRate);
                            }
                            var plntIdx = [];
                            plntIdx[0] = Math.round((Math.random() * 9876533) % endDrop);
                            plntIdx[1] = Math.round((Math.random() * 4332123) % endDrop);
                            plntIdx[2] = Math.round((Math.random() * 7675913) % endDrop);
                            var plantDrop = [];
                            for (var roundRand = 0; roundRand < 3; roundRand++) {
                                for (var i = 0; i < dataRatio.length; i++) {
                                    if (plntIdx[roundRand] >= dataRatio[i].startDrop && plntIdx[roundRand] <= dataRatio[i].endDrop) {
                                        plantDrop.push({
                                            'flowerID': dataRatio[i].flowerID,
                                            'randomIdx': plntIdx[roundRand]
                                        });
                                        break;
                                    }
                                }
                            }
                            if (!sesDocs.data) {
                                sesDocs.data = {};
                            }
                            sesDocs.data.plantRandom = plantDrop;
                            Session.update({
                                    'sessionId': sessionId
                                }, //where
                                sesDocs, //set data
                                function(err, result) {
                                    if (!err) {
                                        res.json(new Message(req.url, 'Y', plantDrop));
                                    } else {
                                        throw new Error('Internal Server Error, can not update session');
                                    }
                                }
                            )
                        }
                    });
                }
            }
        });
    } catch (err) {
        logger(req.url, err);
        const errorData = err.message | 'UnknownError';
        res.status(500).json(new Message(req.url, 'error', errorData));
    }
});

router.put('/plant/random/:selectedIdx', function(req, res) {
    try {
        var sessionId = req.body.sessionId;
        var selectedIdx = req.params.selectedIdx;

        Session.findOne({
            'sessionId': sessionId
        }, function(err, sesDocs) {
          if(err || !sesDocs){
              throw new Error('Internal Server Error, not found sessionId');
          }
            var plantRandomTemp;
            if (sesDocs.data) {
                if (sesDocs.data.plantRandom) {
                    plantRandomTemp = sesDocs.data.plantRandom;
                }
            }
            if (plantRandomTemp) {
                var selectedPlant = plantRandomTemp[selectedIdx % plantRandomTemp.length];
                if (selectedPlant) {
                    var sessionUpdateFlag = false;
                    var userUpdateFlag = false;
                    delete sesDocs.data.plantRandom;
                    console.log(sesDocs);
                    Session.update({'sessionId': sesDocs.sessionId}, {'data': sesDocs.data}, function(err, result){
                      if (err) {
                          throw new Error('Internal Server Error, can not update session');
                      } else {
                        sessionUpdateFlag = true;
                        updateFlag(sessionUpdateFlag, userUpdateFlag);
                      }
                    });
                    User.findOne({
                        'username': sesDocs.username
                    }, function(err, userDoc) {
                        if (err || !userDoc) {
                            throw new Error('Internal Server Error, can not get user data');
                        } else {
                            userDoc.flowerStatus.push({
                                'flowerID': selectedPlant.flowerID,
                                'classLevel': 0,
                                'walkingDistance': 0,
                                'numberOfFeeding': 0
                            });
                            userDoc.save(function(err) {
                                if (err) {
                                    throw new Error('Internal Server Error, can not insert flowerStatus to user');
                                } else {
                                    userUpdateFlag = true;
                                    updateFlag(sessionUpdateFlag, userUpdateFlag);
                                }
                            });
                        }
                    });

                    function updateFlag(sesFlag, userFlag) {
                      logger(req.url, 'check flag('+sesFlag+'|'+userFlag+')');
                        if (sesFlag && userFlag) {
                            User.findOne({
                                'username': sesDocs.username
                            }, function(err, userDoc) {
                                res.json(new Message(req.url, 'Y', userDoc));
                            });
                        }
                    }
                } else {
                    res.json(new Message(req.url, 'N', 'not found selected plant!'));
                }
            } else {
                res.json(new Message(req.url, 'N', 'not found plant random in session, please random first!'));
            }
        });

    } catch (err) {
        logger(req.url, err);
        const errorData = err.message | 'UnknownError';
        res.status(500).json(new Message(req.url, 'error', errorData));
    }
});

router.put('/user/sync', function(req, res){
  var sessionId = req.body.sessionId;
  Session.findOne({'sessionId': sessionId}, function(err, sesDocs){

  });
});

router.put('/user/collection/add', function(req, res){
  var sessionId = req.body.sessionId;
  Session.findOne({'sessionId': sessionId}, function(err, sesDocs){

  });
});
module.exports = router;
