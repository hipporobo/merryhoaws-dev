const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = require('../models/User');
const Session = require('../models/Session');
const Message = require('../core/message-response');
const logger = require('../core/logger');

const router = express.Router();

var SALT = 'MERRYHOA';
var SECRET = 'MerryHoaGame';

router.get('/test', function(req, res){
  try{
    throw new Error('test error');
  }catch(err){
    res.status(500).json(new Message(req.url, err.message, null));
  }
});

router.post('/testDecode', function(req, res){
    var sessionId = req.body.sessionId;
    var decoded = jwt.decode(sessionId);
    res.json(decoded);
});

router.post('/login', function(req, res){
  try{
    var sessionId;
    var username;
    var password;
    if(!req.body.sessionId){
        if(!req.body.username)throw new Error('not found "username"');
        if(!req.body.password)throw new Error('not found "password"');

        username = req.body.username;
        password = req.body.password;
    }else{
        sessionId = req.body.sessionId;
        const decoded = jwt.decode(sessionId);

        username = decoded.user.username;
        password = decoded.user.password;
    }

    User.findOne({'username': username}, callbackLogin);
    function callbackLogin(err, docs){
      if(!err){
        logger('docs', docs);
        if(docs && ((docs.password == password)||(docs.password == new Buffer(SALT+password).toString('base64')))){
          sessionId = jwt.sign({user: docs}, SECRET, {
            expiresIn: 3600 // expires in 24 hours
          });
          username = docs.username;
          Session.findOne({'username': docs.username}, function(err, sesdocs){
            if(!err){
              if(sesdocs){
                logger('update session', sesdocs);
                if(!sesdocs.data){
                  sesdocs.data = {};
                }
                Session.update(
                  {'username': docs.username},//where
                  {'sessionId': sessionId, 'loginDate': new Date(), 'data': sesdocs.data},//set data
                  callbackUpdateSession//handle result
                );
              }else{
                logger('create session', docs);
                const session = new Session({
                  'sessionId': sessionId,
                  'username': docs.username,
                  'loginDate': new Date(),
                  'data': {}
                });
                session.save(callbackCreateSession);
              }
            }else{
              logger('callbackFineSession', err);
              res.status(500).json(new Message(req.url, 'error', 'Internal Server Error, can not login!, '+err.message));
            }
          });
        }else{
          res.json(new Message(req.url, 'N', 'can not login username or password are wrong!'));
        }
      }else{
        logger('callbackLogin', err);
        res.status(500).json(new Message(req.url, 'error', 'Internal Server Error, can not login!, '+err.message));
      }
    }

    function callbackCreateSession(err, docs){
      if(!err){
        res.json(new Message(req.url, 'Y', {username: username, sessionId: sessionId}));
      }else{
        throw new Error('Internal Server Error, can not create session, '+err.message);
      }
    }

    function callbackUpdateSession(err, docs){
      if(!err){
        res.json(new Message(req.url, 'Y', {username: username, sessionId: sessionId}));
      }else{
        throw new Error('Internal Server Error, can not update session, '+err.message);
      }
    }

  }catch(err){
    logger('/login', err);
    const errorData = err.message|'UnknownError';
    res.status(500).json(new Message(req.url, 'error', errorData));
  }
});



router.post('/register', function(req, res){
  try{
    //validate value is not empty
    if(!req.body.username)throw new Error('not found "username"');
    if(!req.body.password)throw new Error('not found "password"');
    if(!req.body.displayName)throw new Error('not found "displayName"');

    //parse value to object
    const user = new User({
      username: req.body.username,
      password: new Buffer(SALT+req.body.password).toString('base64'),
      displayName: req.body.displayName
    });

    //insert data
    user.save(function(error, docs){
      if(error){
        logger(req.url, error);
        res.status(500).json(new Message(req.url, 'error', 'can not insert data, Cause:"'+error.message+'"'));
      }else{
        res.json(new Message(req.url, 'Y', 'insert data success'));
      }
    });
  }catch(err){
    const errorData = err.message|'UnknownError';
    res.status(500).json(new Message(req.url, 'error', errorData));
  }
});

module.exports = router;
