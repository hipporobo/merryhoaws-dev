const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = require('../models/User');
const Message = require('../core/message-response');
const logger = require('../core/logger');

const router = express.Router();

var SECRET = 'MerryHoaGame';




router.get('/', function(req, res) {
    User.find({}, 'username displayName collectionFlowers flowerStatus', function(err, docs) {
        if (err) {
            res.status(500).json(new Message(req.url, 'error', 'can not get data'));
        } else {
            res.json(new Message(req.url, 'Y',docs));
        }
    });
});


router.get('/:username', function(req, res) {
    const username = req.params.username;
    User.findOne({
            'username': username
        }, 'username displayName collectionFlowers flowerStatus',
        function(err, docs) {
            if (err) {
                res.status(500).json(new Message(req.url, 'error', 'can not get data'));
            } else {
                res.json(new Message(req.url, 'Y',docs));
            }
        });
});


module.exports = router;
