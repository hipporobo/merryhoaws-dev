const express = require('express');
const mongoose = require('mongoose');
const uuid = require('uuid');

const User = require('../models/User');
const Plant = require('../models/Plant');
const Message = require('../core/message-response');
const logger = require('../core/logger');

const router = express.Router();

var SECRET = 'MerryHoaGame';

router.get('/', function(req, res) {
    Plant.find({}, function(err, docs) {
        if (err) {
            logger(req.url, error);
            res.status(500).json(new Message(req.url, 'error', 'can not get data, Cause:"' + error.message + '"'));
        } else {
            res.json(new Message(req.url, 'Y', docs));
        }
    });
});

router.get('/flower/:flowerID', function(req, res) {
    try {
        const flowerID = req.params.flowerID;
        Plant.findOne({
            'flowerID': flowerID
        }, function(err, docs) {
            if (err) {
                res.status(500).json(new Message(req.url, 'error', 'can not get data'));
            } else {
                res.json(new Message(req.url, 'Y', docs));
            }
        });
    } catch (err) {
        const errorData = err.message | 'UnknownError';
        res.status(500).json(new Message(req.url, err.message, errorData));
    }
});

router.post('/add', function(req, res) {
    try {
        if (!req.body.flowerID) throw new Error('not found "flowerID"');
        if (!req.body.name) throw new Error('not found "name"');
        if (!req.body.plantType) throw new Error('not found "plantType"');
        if (!req.body.detail) throw new Error('not found "detail"');
        if (!req.body.totalClassLevel) throw new Error('not found "totalClassLevel"');
        if (!req.body.lifeTime) throw new Error('not found "lifeTime"');
        if (!req.body.burnDistance) throw new Error('not found "burnDistance"');
        if (!req.body.dropRate) throw new Error('not found "dropRate"');

        var plant = new Plant({
            'flowerID': req.body.flowerID,
            'name': req.body.name,
            'plantType': req.body.plantType,
            'detail': req.body.detail,
            'totalClassLevel': req.body.totalClassLevel,
            'lifeTime': req.body.lifeTime,
            'burnDistance': req.body.burnDistance,
            'dropRate': req.body.dropRate
        });

        plant.save(function(error, docs) {
            if (error) {
                logger(req.url, error);
                throw new Error('Error: can not insert data\tCause:' + error.message);
            } else {
                res.json(new Message(req.url, 'Y', 'insert data success'));
            }
        });
    } catch (err) {
        const errorData = err.message || 'UnknownError';
        res.status(500).json(new Message(req.url, 'error', errorData));
    }
});


module.exports = router;
