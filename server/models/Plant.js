var mongoose = require('mongoose');

var PlantSchema = mongoose.Schema(
  {
    flowerID: {
      type: String,
      required: true,
      index: {
        unique: true
      }
    },
    name: String,
    plantType: String,
    detail: String,
    totalClassLevel: Number,
    lifeTime: Number,
    burnDistance: Number,
    dropRate: Number
  }
);

module.exports = mongoose.model('Plant', PlantSchema, 'plants');
