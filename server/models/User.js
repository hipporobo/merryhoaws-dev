var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    password: {
        type: String,
        required: true
    },
    displayName: String,
    registerDate: {
        type: Date,
        default: Date.now
    },
    flowerStatus: [{
        flowerID: String,
        classLevel: Number,
        walkingDistance: Number,
        numberOfFeeding: Number
    }],
    collectionFlowers: [{
        flowerID: String,
        countOfPlanting: Number
    }]
});

module.exports = mongoose.model('User', UserSchema, 'users');
