var mongoose = require('mongoose');

var SessionSchema = mongoose.Schema(
  {
    sessionId: String,
    username: String,
    loginDate: Date,
    data: {}
  }
);

module.exports = mongoose.model('Session', SessionSchema, 'sessions');
