//import file
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var utils = require('./server/core/utils');
var authens = require('./server/api/authen');
var users = require('./server/api/users');
var plants = require('./server/api/plant')
var games = require('./server/api/games');

var port = 3000;
// ตัวแปร app สำหรับรัน server
var app = express();

mongoose.connect('mongodb://localhost:27017/merryhoadb');

app.use(bodyParser.json());
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));
app.use(bodyParser.urlencoded({
    extended: true
}));

//logger router
app.use(function(req, res, next) {
    console.log(
        utils.getLogTime(),
        new String(req.method).toUpperCase(),
        req.url);
    next();
});
//set api

app.use('/api/authens', authens);
app.use('/api/plants', plants);
app.use('/api/users', users);
app.use('/api/games', games);

app.get('/api/version', function(req, res) {
    res.json({
        'serverName': 'Merry Hoa Web Service',
        'serverVersion:': '0.0.1',
        'routs': {
            '/api/authens': authens.stack,
            '/api/plants': plants.stack,
            '/api/users': users.stack,
            '/api/games': games.stack
        }
    });
});

app.use(function(req, res) {
    console.log(
        utils.getLogTime(),
        new String(req.method).toUpperCase(),
        req.url,
        'response');
});



//รันเซิร์ฟเวอร์
app.listen(port);

//log บอกว่ารันเซิร์ฟเวอร์
console.log(utils.getLogTime(), 'WEB is running on port ', port);


//exports โมดูล **ไม่ต้องสนใจ
exports = module.exports = app;
